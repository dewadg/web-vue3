FROM node:lts-alpine AS build

WORKDIR /build

ADD . .

RUN yarn
RUN yarn build

FROM nginx

ADD ./deploy/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /build/dist/index.html /var/www/html/index.html
COPY --from=build /build/dist/index.html.gz /var/www/html/index.html.gz
