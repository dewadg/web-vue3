const S3Plugin = require('webpack-s3-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const env = require('./build-env.json')

module.exports = {
  configureWebpack: {
    plugins: [
      new CompressionPlugin(),
      new S3Plugin({
        exclude: /.*\.html$/,
        s3Options: {
          accessKeyId: env.SPACE_ACCESS_KEY_ID,
          secretAccessKey: env.SPACE_SECRET_ACCESS_KEY,
          region: env.SPACE_REGION,
          endpoint: env.SPACE_ENDPOINT
        },
        s3UploadOptions: {
          Bucket: env.SPACE_BUCKET
        },
        cdnizerOptions: {
          defaultCDNBase: env.SPACE_CDN_BASE
        }
      })
    ]
  }
}
