import { ref, reactive } from 'vue'

export function useLatestTweet () {
  const isLatestTweetLoading = ref(false)
  const latestTweetError = ref('')
  
  const latestTweet = reactive({
    text: '',
    user: ''
  })

  const fetchLatestTweet = async () => {
    const payload = {
      operationName: null,
      query: `
        query ($user: String) {
          latestTweet (user: $user) {
            text
            user {
              screenName
            }
          }
        }
      `,
      variables: {
        user: 'dewadg'
      }
    }
    
    try {
      isLatestTweetLoading.value = true
      const response = await fetch('https://twtx.dewadg.pro/query', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      })
      const { data } = await response.json()

      latestTweet.text = data.latestTweet.text
      latestTweet.user = data.latestTweet.user.screenName
    } catch (error) {
      latestTweetError.value = error.message
    } finally {
      isLatestTweetLoading.value = false
    }
  }

  return {
    isLatestTweetLoading,
    latestTweet,
    latestTweetError,
    fetchLatestTweet
  }
}
